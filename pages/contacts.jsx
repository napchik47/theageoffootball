import React from 'react';
import Head from 'next/head';
import { addApolloState, initializeApollo } from '@lib/apolloClient';
import { useTranslations } from 'next-intl';
import { Box, Divider, Heading, Text } from '@chakra-ui/react';
import Header from '@components/header';
import Container from '@components/container';
import Footer from '@components/footer';
import { PRIMARY_MENU } from '@components/menu-items/menu-items';

const Contacts = () => {
  const t = useTranslations();

  return (
    <>
      <Head>
        <title>{t('Common.news')} - TheAgeOfFootball</title>
        <meta
          name="description"
          content={`TheAgeOfFootball - ${t('Common.siteDescription')}`}
        />
        <meta
          property="og:image"
          content="https://theageoffootball.com/logo-light.svg"
        />
        <meta property="og:url" content="https://theageoffootball.com" />
        <meta property="og:site_name" content="TheAgeOfFootball" />
        <meta
          property="og:title"
          content={`${t('Common.news')} - TheAgeOfFootball`}
        />
        <meta property="og:locale" content="ru" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@TheAgeOfFootball" />
        <meta name="twitter:creator" content="@TheAgeOfFootball" />
      </Head>
      <Header />
      <Box mb={20}>
        <Box as="section" pb={{ base: '0', md: '3rem' }}>
          <Container>
            <Box textAlign="center">
              <Heading as="h1" textAlign="center" mb="10">
                {t('Contacts.contactUs')}
              </Heading>
              <Text>
                <strong>{t('Contacts.chiefEditor')}</strong> Володимир Медяник —
                medianyk_volodymyr@yahoo.com
              </Text>
              <Text>
                <strong>{t('Contacts.email')}</strong> —
                theageoffootball.com@gmail.com
              </Text>
              <Text>
                <strong>{t('Contacts.adsQuestions')}</strong>:
                medianyk_volodymyr@yahoo.com
              </Text>
            </Box>
          </Container>
        </Box>

        <Divider />

        <Footer />
      </Box>
    </>
  );
};

export async function getStaticProps() {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: PRIMARY_MENU,
  });

  return addApolloState(apolloClient, {
    props: {
      messages: require(`../locales/ru.json`),
    },
  });
}

export default Contacts;
