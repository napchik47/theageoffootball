import Image from 'next/image';
import { gql, useQuery } from '@apollo/client';
import { Box } from '@chakra-ui/react';

import PageContainer from '@components/page-container';
import Pagination from '@components/pagination';
import { IPostProps } from '@components/post-list/post-list';
import Sidebar from '@components/sidebar/sidebar';
import { addApolloState, initializeApollo } from '@lib/apolloClient';
import { GetStaticPaths, GetStaticPropsContext } from 'next';
import { useEffect } from 'react';
import PostBody from '@components/post-body/post-body';
import { PRIMARY_MENU } from '@components/menu-items/menu-items';
import { BlogSEO } from '@components/seo-meta';

export const POST_AND_MORE_POSTS = gql`
  fragment AuthorFields on User {
    id
    name
    firstName
    lastName
    avatar {
      url
    }
  }
  fragment PostFields on Post {
    id
    databaseId
    title
    excerpt
    slug
    date
    featuredImage {
      node {
        sourceUrl
      }
    }
    author {
      node {
        ...AuthorFields
      }
    }
    categories {
      edges {
        node {
          name
          slug
        }
      }
    }
    tags {
      edges {
        node {
          name
        }
      }
    }
  }
  query postBySlug($id: ID!, $idType: PostIdType!) {
    post(id: $id, idType: $idType) {
      ...PostFields
      content
      nextPost {
        node {
          id
          slug
          title
        }
      }
      previousPost {
        node {
          id
          slug
          title
        }
      }
    }
  }
`;

export const GET_ALL_POSTS_WITH_SLUG = gql`
  query postsPaths {
    posts(first: 200) {
      edges {
        node {
          slug
        }
      }
    }
  }
`;

export const stripTags = (content: string) => {
  const regex = /(<([^>]+)>)/gi;
  return content.replace(regex, '').trim();
};

export const truncateString = (str: string, num: number) => {
  if (str.length <= num) {
    return str;
  }
  return `${str.slice(0, num)}...`;
};

interface IPostPageProps {
  slug: string;
  preview?: string;
}

function Post({ preview, slug }: IPostPageProps) {
  const { loading, error, data } = useQuery(POST_AND_MORE_POSTS, {
    variables: {
      id: slug,
      idType: 'SLUG',
    },
  });

  const post: IPostProps = data?.post;

  useEffect(() => {
    if ((window as any).instgrm) (window as any).instgrm.Embeds?.process();
    if ((window as any).twttr) (window as any).twttr.widgets.load();
  }, [post.id]);

  if (loading) return 'Loading';
  if (error) return 'Error';

  if (!post) return null;

  const author =
    post?.author?.node.firstName && post?.author?.node.lastName
      ? `${post.author.node.firstName} ${post.author.node.lastName}`
      : post?.author?.node.name;

  const hasFeaturedImage = !!post.featuredImage?.node?.sourceUrl;

  const featuredImageMaybe = hasFeaturedImage
    ? [post.featuredImage.node.sourceUrl]
    : [];

  return (
    <>
      {post && (
        <BlogSEO
          title={`${post.title} | theageoffootball news`}
          summary={truncateString(stripTags(post.excerpt), 255).trim()}
          images={[...featuredImageMaybe]}
          author={author}
          date={new Date(post.date)}
        />
      )}

      <PageContainer
        frontmatter={{ title: post.title, slug }}
        sidebar={<Sidebar />}
        pagination={
          <Pagination
            previous={
              post.nextPost && {
                path: `/post/${post.nextPost.node.slug}`,
                title: post.nextPost.node.title,
              }
            }
            next={
              post.previousPost && {
                path: `/post/${post.previousPost.node.slug}`,
                title: post.previousPost.node.title,
              }
            }
          />
        }
      >
        <Box
          mb="32px"
          height={{ base: '15rem', md: '23rem' }}
          pos="relative"
          borderRadius="2xl"
          boxShadow="xl"
          overflow="hidden"
        >
          {hasFeaturedImage && (
            <Image
              src={post.featuredImage.node.sourceUrl}
              layout="fill"
              objectFit="cover"
              loading="eager"
              placeholder="blur"
              alt={post.title}
              blurDataURL="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPUuatYDwAEEgGrsRTQ5QAAAABJRU5ErkJggg=="
            />
          )}
        </Box>

        <PostBody content={post.content || ''} />
      </PageContainer>
    </>
  );
}

export async function getStaticProps({
  params,
}: GetStaticPropsContext<{ slug: string }>) {
  const apolloClient = initializeApollo();

  const slug = params?.slug;

  await Promise.all([
    apolloClient.query({
      query: POST_AND_MORE_POSTS,
      variables: {
        id: slug,
        idType: 'SLUG',
      },
    }),
    apolloClient.query({
      query: PRIMARY_MENU,
    }),
  ]);

  return addApolloState(apolloClient, {
    props: {
      messages: require(`../../locales/ru.json`),
      slug,
    },
    revalidate: 300,
  });
}

export const getStaticPaths: GetStaticPaths = async () => {
  const apolloClient = initializeApollo();

  const {
    data: { posts: allPosts },
  } = await apolloClient.query({
    query: GET_ALL_POSTS_WITH_SLUG,
  });

  return {
    paths:
      allPosts.edges.map(({ node }: { node: any }) => `/post/${node.slug}`) ||
      [],
    fallback: 'blocking',
  };
};

export default Post;
