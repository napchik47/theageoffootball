import Head from 'next/head';
import { Box, Divider } from '@chakra-ui/react';
import { useTranslations } from 'next-intl';

import { addApolloState, initializeApollo } from '@lib/apolloClient';

import Container from '@components/container';
import { Footer } from '@components/footer';
import Header from '@components/header';
import PostList, {
  allPostsQueryVars,
  ALL_POSTS_QUERY,
} from '@components/post-list/post-list';
import { PRIMARY_MENU } from '@components/menu-items/menu-items';

export default function Home() {
  const t = useTranslations('Common');

  return (
    <>
      <Head>
        <title>{t('news')} - TheAgeOfFootball</title>
        <meta
          name="description"
          content={`TheAgeOfFootball - ${t('siteDescription')}`}
        />
        <meta
          property="og:image"
          content="https://theageoffootball.com/logo-light.svg"
        />
        <meta property="og:url" content="https://theageoffootball.com" />
        <meta property="og:site_name" content="TheAgeOfFootball" />
        <meta property="og:title" content={`${t('news')} - theageoffootball`} />
        <meta property="og:locale" content="ru" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@TheAgeOfFootball" />
        <meta name="twitter:creator" content="@TheAgeOfFootball" />
      </Head>

      <Header />
      <Box mb={20}>
        <Box as="section" pb={{ base: '0', md: '3rem' }}>
          <Container>
            <Box textAlign="center">
              <PostList />
            </Box>
          </Container>
        </Box>

        <Divider />

        <Footer />
      </Box>
    </>
  );
}

export async function getStaticProps() {
  const apolloClient = initializeApollo();
  await Promise.all([
    apolloClient.query({
      query: ALL_POSTS_QUERY,
      variables: allPostsQueryVars,
    }),
    apolloClient.query({
      query: PRIMARY_MENU,
    }),
  ]);

  return addApolloState(apolloClient, {
    props: {
      messages: require(`../locales/ru.json`),
    },
    revalidate: 300,
  });
}
