import { SitemapStream, streamToPromise } from 'sitemap';
import { initializeApollo } from '@lib/apolloClient';
import type { NextApiRequest, NextApiResponse } from 'next';
import { IPostProps } from '@components/post-list/post-list';
import { GET_ALL_POSTS_WITH_SLUG } from 'pages/post/[slug]';

const generateSitemap = async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const smStream = new SitemapStream({
      hostname: `https://${req.headers.host}`,
      xslUrl: `https://${req.headers.host}/style.xsl`,
    });

    smStream.write({
      url: '/',
    });

    const apolloClient = initializeApollo();

    const {
      data: { posts: allPosts },
    } = await apolloClient.query({
      query: GET_ALL_POSTS_WITH_SLUG,
    });

    // Create each URL row
    allPosts.edges
      .map(({ node }: { node: IPostProps }) => ({
        url: `/post/${node.slug}`,
        lastmod: node.modified,
      }))
      .forEach(({ url, lastmod }: { url: string; lastmod: string }) => {
        smStream.write({
          url,
          lastmod,
        });
      });

    console.log(smStream);

    // End sitemap stream
    smStream.end();

    // XML sitemap string
    const sitemapOutput = (await streamToPromise(smStream)).toString();

    // Change headers
    res.writeHead(200, {
      'Content-Type': 'application/xml',
    });

    // Display output to user
    res.end(sitemapOutput);
  } catch (e) {
    console.log(e);
    res.send(JSON.stringify(e));
  }
};

export default generateSitemap;
