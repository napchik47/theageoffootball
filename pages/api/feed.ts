/* eslint-disable */
import { gql } from '@apollo/client';
import { allPostsQueryVars } from '@components/post-list/post-list';
import { initializeApollo } from '@lib/apolloClient';
import { format, parseISO } from 'date-fns';

import type { NextApiRequest, NextApiResponse } from 'next';
import { IPostProps } from '@components/post-list/post-list';

export const FEED_QUERY = gql`
  query allPosts($cursor: String, $first: Int) {
    posts(
      first: $first
      after: $cursor
      where: { orderby: { field: DATE, order: DESC } }
    ) {
      edges {
        cursor
        node {
          id
          title
          excerpt
          content
          slug
          date
          featuredImage {
            node {
              sourceUrl
              mimeType
            }
          }
          categories {
            edges {
              node {
                name
              }
            }
          }
        }
      }
    }
  }
`;

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const xmlWrapper = (feedItems: string) => `
    <rss xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/" xmlns:yandex="http://news.yandex.ru" version="2.0">
      <channel>
      <title>Новости - TheAgeOfFootball</title>
      <atom:link href="https://${req.headers.host}/feed/" rel="self" type="application/rss+xml"/>
      <link>https://${req.headers.host}/</link>
      <description>TheAgeOfFootball - Новости, которые тебе понравятся</description>
      <language>ru</language>
      <copyright>Copyright 2020 theageoffootball.com</copyright>
      <image>
        <url>https://${req.headers.host}/favicon/apple-touch-icon.png</url>
        <title>TheAgeOfFootball - Новости, которые тебе понравятся</title>
        <link>https://${req.headers.host}/</link>
        <width>180</width>
        <height>180</height>
      </image>
        ${feedItems}
      </channel>
    </rss>
  `;

  const stripTags = (content: string) => content.replace(/(<([^>]+)>)/gi, '');

  const extractFeedData = (posts: Array<{ node: IPostProps }>) => {
    return posts
      .map(({ node: post }: { node: IPostProps }) => {
        return `
        <item>
          <guid>https://${req.headers.host}/post/${post?.slug}</guid>
          <link>https://${req.headers.host}/post/${post?.slug}</link>
          <title>
          <![CDATA[ ${post?.title} ]]>
          </title>
          <pubDate>${format(
            parseISO(post?.date),
            'E, do MMM yyyy HH:mm:ss +0300'
          )}</pubDate>
          <category>
          <![CDATA[ ${
            post?.categories?.edges.map(
              ({ node: category }) => category?.name
            )?.[0]
          } ]]>
          </category>
          <description>
          <![CDATA[ ${stripTags(post.excerpt)} ]]>
          </description>
          <yandex:full-text>
          <![CDATA[ ${stripTags(post?.content || '')} ]]>
          </yandex:full-text>
          <enclosure url="${post?.featuredImage?.node.sourceUrl}" type="${
          post?.featuredImage?.node.mimeType
        }"/>
        </item>
      `;
      })
      .join('');
  };

  try {
    const apolloClient = initializeApollo();

    const allPosts = await apolloClient.query({
      query: FEED_QUERY,
      variables: allPostsQueryVars({ first: 12 }),
    });

    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/xml; charset=utf-8');
    res.end(xmlWrapper(extractFeedData(allPosts.data.posts.edges)));
  } catch (e) {
    console.log(e);
    res.statusCode = 500;
    res.end();
  }
};
