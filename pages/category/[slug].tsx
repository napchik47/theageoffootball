/* eslint-disable */
import ErrorPage from 'next/error';
import Head from 'next/head';

import { NetworkStatus, useQuery, gql } from '@apollo/client';
import { addApolloState, initializeApollo } from '@lib/apolloClient';
import { PRIMARY_MENU } from '@components/menu-items/menu-items';
import { GetStaticPropsContext } from 'next';
import { useTranslations } from 'next-intl';
import { Box, Button, Divider, Heading } from '@chakra-ui/react';
import Header from '@components/header';
import PostGrid from '@components/post-grid/post-grid';
import Container from '@components/container';
import Footer from '@components/footer';

export const CATEGORY_ARCHIVE_QUERY = gql`
  query categoryArchive($filter: String!, $cursor: String, $first: Int) {
    posts(
      first: $first
      after: $cursor
      where: { categoryName: $filter, orderby: { field: DATE, order: DESC } }
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        cursor
        node {
          id
          title
          excerpt
          slug
          date
          featuredImage {
            node {
              sourceUrl
            }
          }
          author {
            node {
              id
              name
              firstName
              lastName
              avatar {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_ALL_CATEGORY_SLUGS = gql`
  {
    categories {
      edges {
        node {
          slug
        }
      }
    }
  }
`;

export const categoryArchiveQueryVars = ({
  filter,
  first,
  cursor = '',
}: {
  filter: string;
  first: number;
  cursor?: string;
}) => ({
  filter,
  first,
  cursor,
});

interface ICategoryPageProps {
  slug: string;
  preview?: string;
}

function Category({ preview, slug }: ICategoryPageProps) {
  const t = useTranslations('Common');

  const { loading, error, data, fetchMore, networkStatus } = useQuery(
    CATEGORY_ARCHIVE_QUERY,
    {
      variables: categoryArchiveQueryVars({ first: 12, filter: slug }),
      notifyOnNetworkStatusChange: true,
    }
  );

  const loadingMorePosts = networkStatus === NetworkStatus.fetchMore;

  if (error) return <ErrorPage statusCode={404} />;

  const posts = data?.posts?.edges;

  const cursor = data?.posts?.pageInfo.endCursor;

  const loadMorePosts = () => {
    fetchMore({
      variables: categoryArchiveQueryVars({
        filter: slug,
        first: 6,
        cursor,
      }),
    });
  };

  return (
    <>
      <Head>
        <title>{t(`${slug}`)} | TheAgeOfFootball News</title>
      </Head>
      <Header />
      <Box mb={20}>
        <Box as="section" pb={{ base: '0', md: '3rem' }}>
          <Container>
            <Heading as="h1" mb={5} textAlign="center">
              {t(`${slug}`)}
            </Heading>
            <Box textAlign="center">
              {posts && <PostGrid posts={posts} />}
              <div>
                {posts?.length > 0 && (
                  <Button
                    isLoading={loadingMorePosts}
                    loadingText={t('loading') as string}
                    bgGradient="linear(to-l, #90ce7d,#2f5c21))"
                    onClick={loadMorePosts}
                    color="white"
                    colorScheme="brand"
                    variant="solid"
                  >
                    {t('loadMore')}
                  </Button>
                )}
              </div>
            </Box>
          </Container>
        </Box>

        <Divider />

        <Footer />
      </Box>
    </>
  );
}

export async function getStaticProps({
  params,
}: GetStaticPropsContext<{ slug: string }>) {
  const slug = params?.slug || '';

  const apolloClient = initializeApollo();

  await Promise.all([
    apolloClient.query({
      query: CATEGORY_ARCHIVE_QUERY,
      variables: categoryArchiveQueryVars({ filter: slug, first: 12 }),
    }),
    apolloClient.query({
      query: PRIMARY_MENU,
    }),
  ]);

  return addApolloState(apolloClient, {
    props: {
      messages: require(`../../locales/ru.json`),
      slug,
    },
    revalidate: 300,
  });
}

export async function getStaticPaths() {
  const apolloClient = initializeApollo();

  const {
    data: { categories: allCategories },
  } = await apolloClient.query({ query: GET_ALL_CATEGORY_SLUGS });

  return {
    paths:
      allCategories.edges.map(
        ({ node }: { node: any }) => `/category/${node.slug}`
      ) || [],
    fallback: 'blocking',
  };
}

export default Category;
