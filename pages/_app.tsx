import React from 'react';
import { ChakraProvider } from '@chakra-ui/react';
import NextNprogress from 'nextjs-progressbar';
import FontFace from '@components/font-face';
import { useApollo } from '@lib/apolloClient';
import { ApolloProvider } from '@apollo/client';
import { NextIntlProvider } from 'next-intl';
import theme from 'theme';

import type { AppProps } from 'next/app';

import 'react-multi-carousel/lib/styles.css';
import Script from 'next/script';
import { useRouter } from 'next/router';

const App = ({ Component, pageProps }: AppProps) => {
  const apolloClient = useApollo(pageProps);
  const router = useRouter();

  return (
    <>
      <NextNprogress
        color="#f2585c"
        startPosition={0.3}
        stopDelayMs={200}
        height={2}
        showOnShallow={true}
      />
      <NextIntlProvider messages={pageProps.messages} locale="ru">
        <ApolloProvider client={apolloClient}>
          <ChakraProvider resetCSS theme={theme}>
            <Component {...pageProps} />
          </ChakraProvider>
        </ApolloProvider>
      </NextIntlProvider>
      <FontFace />
    </>
  );
};

export default App;
