import { GetServerSideProps } from 'next';
import { Feed } from 'feed';
import { format, parseISO } from 'date-fns';
import { initializeApollo } from '@lib/apolloClient';
import {
  allPostsQueryVars,
  ALL_POSTS_QUERY,
  IPostProps,
} from '@components/post-list/post-list';

const hostUrl = 'https://theageoffootball.com';

const buildFeed = (items: Array<{ node: IPostProps }>) => {
  const feed = new Feed({
    id: hostUrl,
    link: hostUrl,
    title: 'TheAgeOfFootball',
    description: 'TheAgeOfFootball - Новини, які тобі сподобаються',
    copyright: '© TheAgeOfFootball',
    updated: parseISO(items[0].node.date),
    author: {
      name: 'TheAgeOfFootball',
      link: hostUrl,
    },
  });

  items.forEach(({ node: item }) => {
    feed.addItem({
      title: item.title,
      link: `${hostUrl}/post/${item.slug}`,
      description: item.excerpt,
      date: parseISO(item.date),
      content: item.content,
      category: item.categories?.edges.map((cat) => ({ name: cat.node.name })),
      author: [{ name: item.author.node.name }],
      image: item.featuredImage?.node?.sourceUrl,
    });
  });

  return feed;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  if (context && context.res) {
    const { res } = context;

    const apolloClient = initializeApollo();

    const {
      data: {
        posts: { edges: allPosts },
      },
    } = await apolloClient.query<{
      posts: { edges: Array<{ node: IPostProps }> };
    }>({
      query: ALL_POSTS_QUERY,
      variables: allPostsQueryVars({ first: 200 }),
    });

    const feed = buildFeed(allPosts);
    res.setHeader('content-type', 'text/xml');
    res.write(feed.rss2()); // NOTE: You can also use feed.atom1() or feed.json1() for other feed formats
    res.end();
  }

  return {
    props: {},
  };
};

const RssPage = () => null;

export default RssPage;
