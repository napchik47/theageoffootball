const withPWA = require('next-pwa');
const runtimeCaching = require('next-pwa/cache');
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer(
  withPWA({
    swcMinify: true,
    images: {
      domains: [
        'theageoffootball.com',
        's3.eu-central-1.amazonaws.com',
        'secure.gravatar.com',
      ],
    },
    pwa: {
      dest: 'public',
      disable: process.env.NODE_ENV === 'development',
      runtimeCaching,
    },
    async rewrites() {
      return [
        {
          source: '/sitemap.xml',
          destination: '/api/posts-sitemap',
        },
        {
          source: '/feed',
          destination: '/api/feed',
        },
      ];
    },
  })
);
