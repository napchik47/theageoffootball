/* eslint-disable */
import ErrorPage from 'next/error';
import { gql, useQuery, NetworkStatus } from '@apollo/client';

import { Button } from '@chakra-ui/react';
import { useTranslations } from 'next-intl';
import PostGrid from '@components/post-grid/post-grid';
import PostCarousel from '@components/post-carousel/post-carousel';

export const ALL_POSTS_QUERY = gql`
  query allPosts($cursor: String, $first: Int) {
    posts(
      first: $first
      after: $cursor
      where: { orderby: { field: DATE, order: DESC } }
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        cursor
        node {
          id
          databaseId
          title
          excerpt
          slug
          date
          content
          featuredImage {
            node {
              sourceUrl
              srcSet
            }
          }
          author {
            node {
              id
              name
              firstName
              lastName
              avatar {
                url
              }
            }
          }
          categories {
            edges {
              node {
                name
                slug
              }
            }
          }
        }
      }
    }
  }
`;

interface IAllPostsQuery {
  first: number;
  cursor?: string;
}

export const allPostsQueryVars = ({ first, cursor = '' }: IAllPostsQuery) => ({
  first,
  cursor,
});

interface IFeaturedImage {
  sourceUrl: string;
  srcSet: string;
  mimeType?: string;
}

export interface IPostProps {
  id: string;
  databaseId: string;
  title: string;
  excerpt: string;
  slug: string;
  date: string;
  featuredImage: {
    node: IFeaturedImage;
  };
  author: {
    node: {
      id: string;
      name: string;
      firstName: string;
      lastName: string;
      avatar: {
        url: string;
      };
    };
  };
  content?: string;
  nextPost?: {
    node: {
      id: string;
      title: string;
      slug: string;
    };
  };
  previousPost?: {
    node: {
      id: string;
      title: string;
      slug: string;
    };
  };
  modified?: string;
  categories?: {
    edges: Array<{ node: { name: string } }>;
  };
}

export default function PostList() {
  const t = useTranslations('Common');

  const { loading, error, data, fetchMore, networkStatus } = useQuery(
    ALL_POSTS_QUERY,
    {
      variables: allPostsQueryVars({ first: 14 }),
      notifyOnNetworkStatusChange: true,
    }
  );

  const posts = data?.posts?.edges;

  const loadingMorePosts = networkStatus === NetworkStatus.fetchMore;

  const cursor = data?.posts?.pageInfo.endCursor;

  const loadMorePosts = () => {
    fetchMore({
      variables: allPostsQueryVars({ first: 12, cursor }),
    });
  };

  if (error) return <ErrorPage statusCode={404} />;

  return (
    <>
      {posts && <PostCarousel posts={posts} />}

      {posts && <PostGrid posts={posts} />}

      <div>
        {posts?.length > 0 && (
          <Button
            isLoading={loadingMorePosts}
            loadingText={t('loading') as string}
            bgGradient="linear(to-l, #90ce7d, #2f5c21)"
            onClick={loadMorePosts}
            color="white"
            colorScheme="brand"
            variant="solid"
          >
            {t('loadMore')}
          </Button>
        )}
      </div>
    </>
  );
}
