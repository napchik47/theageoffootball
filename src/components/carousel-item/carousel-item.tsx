import React from 'react';
import { Box, Heading, Stack, Text, useColorModeValue } from '@chakra-ui/react';
import Image from 'next/image';
import { IPostProps } from '@components/post-list/post-list';
import { Author } from '@components/author';
import Link from 'next/link';

function CarouselItem(props: IPostProps & { priority: boolean }) {
  return (
    <Link href="/post/[slug]" as={`/post/${props.slug}`}>
      <a aria-label={props.title}>
        <Stack
          textAlign="initial"
          bg={useColorModeValue('white', 'gray.700')}
          rounded="xl"
          overflow="hidden"
          direction={['column', 'column', 'column', 'row']}
          height={{ base: '100%' }}
        >
          <Box
            h={{ base: '12rem', md: '320px' }}
            w="full"
            bg="gray.100"
            pos="relative"
            roundedTopLeft={{ base: 'xl' }}
            roundedTopRight={{ base: 'xl', md: 'none' }}
            roundedBottomLeft={{ base: 'none', md: 'xl' }}
            roundedBottomRight={{ base: 'none', md: 'none' }}
            overflow="hidden"
          >
            <Image
              src={props.featuredImage?.node?.sourceUrl}
              layout="fill"
              objectFit="cover"
              alt={props.title}
              priority={props.priority}
              placeholder="blur"
              blurDataURL="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPUuatYDwAEEgGrsRTQ5QAAAABJRU5ErkJggg=="
            />
          </Box>
          <Stack
            justify="space-betwePen"
            bg={useColorModeValue('white', 'gray.700')}
            mt={0}
            roundedTopLeft={{ base: 'none', md: 'none' }}
            roundedBottomLeft={{ base: 'xl', md: 'none' }}
            roundedTopRight={{ base: 'none', md: 'xl' }}
            roundedBottomRight={{ base: 'xl' }}
          >
            <Stack p={3}>
              <Heading
                color={useColorModeValue('gray.700', 'white')}
                fontSize="2xl"
                fontFamily="body"
              >
                {props.title}
              </Heading>
              <Text
                color="gray.500"
                dangerouslySetInnerHTML={{ __html: props.excerpt }}
              />
            </Stack>

            <Stack p={4} mt="auto" direction="row" align="center">
              <Author {...props} />
            </Stack>
          </Stack>
        </Stack>
      </a>
    </Link>
  );
}

export default CarouselItem;
