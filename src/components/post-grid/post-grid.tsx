import PageTransition from '@components/page-transition';
import PostCard from '@components/post-card/post-card';
import { IPostProps } from '@components/post-list/post-list';
import React from 'react';
import Masonry from 'react-masonry-css';
import css from './post-grid.module.css';

const breakpointColumnsObj = {
  default: 4,
  992: 3,
  768: 2,
  576: 1,
};

const PostGrid = ({ posts }: { posts: Array<{ node: IPostProps }> }) => {
  return (
    <PageTransition>
      <Masonry
        breakpointCols={breakpointColumnsObj}
        className={css.masonryGrid}
        columnClassName={css.masonryGridColumn}
      >
        {posts?.map(({ node: postProps }: { node: IPostProps }) => (
          <PostCard key={postProps.id} {...postProps} />
        ))}
      </Masonry>
    </PageTransition>
  );
};

export default PostGrid;
