/* eslint-disable react-hooks/rules-of-hooks */
import * as React from 'react';
import { useScrollSpy } from '@hooks/use-scrollspy';
import { Heading } from '@utils/get-headings';
import {
  Box,
  ListItem,
  OrderedList,
  chakra,
  Text,
  useColorModeValue,
  BoxProps,
} from '@chakra-ui/react';

interface SidebarRightProps extends BoxProps {}

function SidebarRight(props: SidebarRightProps) {
  return (
    <Box
      as="nav"
      aria-labelledby="toc-title"
      maxWidth="300px"
      flexShrink={0}
      display={{ base: 'none', xl: 'block' }}
      position="sticky"
      py="10"
      pr="4"
      top="6rem"
      right="0"
      fontSize="sm"
      alignSelf="start"
      maxHeight="calc(100vh - 8rem)"
      overflowY="auto"
      sx={{ overscrollBehavior: 'contain' }}
      {...props}
    >
      <div data-la-block="1d70632e-bb8a-4b71-80d5-9348a93bdf82" />
    </Box>
  );
}

export default SidebarRight;
