/* eslint-disable */
import React from 'react';
import ErrorPage from 'next/error';
import { useQuery, NetworkStatus } from '@apollo/client';
import { format, parseISO } from 'date-fns';
import { localizedFormat } from './author';
import {
  allPostsQueryVars,
  ALL_POSTS_QUERY,
  IPostProps,
} from './post-list/post-list';
import { useTranslations } from 'next-intl';
import { Button, chakra, Flex, List, ListItem } from '@chakra-ui/react';
import SidebarLink from './sidebar/sidebar-link';

export default function PostFeed() {
  const { loading, error, data, fetchMore, networkStatus } = useQuery(
    ALL_POSTS_QUERY,
    {
      variables: allPostsQueryVars({ first: 12 }),
      notifyOnNetworkStatusChange: true,
    }
  );

  const t = useTranslations('Common');

  const loadingMorePosts = networkStatus === NetworkStatus.fetchMore;

  const posts = data?.posts?.edges;
  const cursor = data?.posts?.pageInfo.endCursor;

  const loadMorePosts = () => {
    fetchMore({
      variables: allPostsQueryVars({ first: 12, cursor }),
    });
  };

  if (error) return <ErrorPage statusCode={404} />;

  // this gives an object with dates as keys
  const groups: { [key: string]: Array<IPostProps> } = posts?.reduce(
    (groups: any, { node }: { node: IPostProps }) => {
      const date = node.date.split('T')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(node);
      return groups;
    },
    {}
  );

  return (
    <chakra.div>
      <>
        {posts &&
          Object.entries(groups || {}).map(([date, posts]) => (
            <chakra.div key={date}>
              <chakra.h3
                fontSize="md"
                fontWeight="extrabold"
                my="1.25rem"
                textTransform="uppercase"
                letterSpacing="wider"
                color="brand.300"
                textAlign="center"
              >
                {localizedFormat(parseISO(date), 'd MMMM')}
              </chakra.h3>
              <List spacing={3}>
                {posts.map((post, i: number) => (
                  <ListItem key={i}>
                    <SidebarLink href="/post/[slug]" as={`/post/${post.slug}`}>
                      <chakra.h4
                        fontSize="xs"
                        fontWeight="bold"
                        my="0.3rem"
                        textTransform="uppercase"
                        letterSpacing="wider"
                      >
                        <chakra.span
                          fontSize="sm"
                          fontWeight="extrabold"
                          color="brand.300"
                        >
                          {format(parseISO(post.date), 'HH:mm')}
                        </chakra.span>{' '}
                        {post.title}
                      </chakra.h4>
                    </SidebarLink>
                  </ListItem>
                ))}
              </List>
            </chakra.div>
          ))}
      </>
      {posts?.length > 0 && (
        <Flex justifyContent="center">
          <Button
            isLoading={loadingMorePosts}
            loadingText={t('loading') as string}
            onClick={loadMorePosts}
            color="brand.300"
            variant="outline"
            size="sm"
          >
            {t('loadMore')}
          </Button>
        </Flex>
      )}
    </chakra.div>
  );
}
