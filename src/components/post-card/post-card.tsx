import {
  Avatar,
  Box,
  Center,
  Heading,
  Stack,
  Text,
  useColorModeValue,
} from '@chakra-ui/react';
import { Author } from '@components/author';
import { IPostProps } from '@components/post-list/post-list';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

function PostCard(
  props: IPostProps & { image?: { img: string; base64: string } }
) {
  return (
    <Stack
      textAlign="initial"
      p={1}
      bgGradient="linear(to-l, #90ce7d, #2f5c21)"
      rounded="2xl"
    >
      <Link href="/post/[slug]" as={`/post/${props.slug}`}>
        <a aria-label={props.title}>
          <Box
            w="full"
            bg={useColorModeValue('white', 'gray.900')}
            boxShadow="2xl"
            rounded="xl"
            overflow="hidden"
          >
            <Box h="210px" bg="gray.100" pos="relative">
              {props.featuredImage && (
                <Image
                  src={props.featuredImage?.node?.sourceUrl}
                  layout="fill"
                  objectFit="cover"
                  alt={props.title}
                  placeholder="blur"
                  blurDataURL="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPUuatYDwAEEgGrsRTQ5QAAAABJRU5ErkJggg=="
                />
              )}
            </Box>
            <Stack p={4}>
              <Heading
                color={useColorModeValue('gray.700', 'white')}
                fontSize="2xl"
                fontFamily="body"
              >
                {props.title}
              </Heading>
              <div
                color="gray.500"
                dangerouslySetInnerHTML={{ __html: props.excerpt }}
              />
            </Stack>
            <Stack p={4} mt={6} direction="row" spacing={4} align="center">
              <Author {...props} />
            </Stack>
          </Box>
        </a>
      </Link>
    </Stack>
  );
}

export default PostCard;
