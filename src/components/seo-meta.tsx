import Head from 'next/head';
import { useRouter } from 'next/router';
import siteConfig from 'configs/site-config';

interface ICommonSEOProps {
  title: string;
  description: string;
  ogType: string;
  ogImage: Array<{ url: string }> | string;
  twImage: string;
}

const CommonSEO = ({
  title,
  description,
  ogType,
  ogImage,
  twImage,
}: ICommonSEOProps) => {
  const router = useRouter();
  return (
    <Head>
      <title>{title}</title>
      <meta name="robots" content="follow, index" />
      <meta name="description" content={description} />
      <meta
        property="og:url"
        content={`${siteConfig.seo.siteUrl}${router.asPath}`}
      />
      <meta property="og:type" content={ogType} />
      <meta property="og:site_name" content={siteConfig.seo.title} />
      <meta property="og:description" content={description} />
      <meta property="og:title" content={title} />
      {Array.isArray(ogImage) ? (
        ogImage.map(({ url }) => (
          <meta property="og:image" content={url} key={url} />
        ))
      ) : (
        <meta property="og:image" content={ogImage} key={ogImage} />
      )}
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:site" content={siteConfig.seo.twitter.site} />
      <meta name="twitter:title" content={`${title} | theageoffootball`} />
      <meta name="twitter:description" content={description} />
      <meta name="twitter:image" content={twImage} />
    </Head>
  );
};

interface IBlogSEOProps {
  author: string;
  title: string;
  summary: string;
  date: Date;
  lastmod?: string;
  images: string[] | string;
}

export const BlogSEO = ({
  author,
  title,
  summary,
  date,
  lastmod,
  images = [],
}: IBlogSEOProps) => {
  const router = useRouter();
  const publishedAt = new Date(date).toISOString();
  const modifiedAt = new Date(lastmod || date).toISOString();
  let imagesArr =
    images.length === 0
      ? [siteConfig.seo.socialBanner]
      : typeof images === 'string'
      ? [images]
      : images;

  const featuredImages = imagesArr.map((img) => {
    return {
      '@type': 'ImageObject',
      url: img,
    };
  });

  const authorList = {
    '@type': 'Person',
    name: author ? author : siteConfig.author,
  };

  const structuredData = {
    '@context': 'https://schema.org',
    '@type': 'Article',
    mainEntityOfPage: {
      '@type': 'WebPage',
      '@id': `${siteConfig.seo.siteUrl}${router.asPath}`,
    },
    headline: title,
    image: featuredImages,
    datePublished: publishedAt,
    dateModified: modifiedAt,
    author: authorList,
    publisher: {
      '@type': 'Organization',
      name: siteConfig.author,
      logo: {
        '@type': 'ImageObject',
        url: siteConfig.seo.siteLogo,
      },
    },
    description: summary,
  };

  const twImageUrl = featuredImages[0].url;

  return (
    <>
      <CommonSEO
        title={title}
        description={summary}
        ogType="article"
        ogImage={featuredImages}
        twImage={twImageUrl}
      />
      <Head>
        {date && (
          <meta property="article:published_time" content={publishedAt} />
        )}
        {lastmod && (
          <meta property="article:modified_time" content={modifiedAt} />
        )}
        <link
          rel="canonical"
          href={`${siteConfig.seo.siteUrl}${router.asPath}`}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(structuredData, null, 2),
          }}
        />
      </Head>
    </>
  );
};
