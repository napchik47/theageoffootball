import { useRouter } from 'next/router';
import * as React from 'react';
import { Box, Button, chakra, Flex, Heading, Link } from '@chakra-ui/react';
// import { SkipNavContent, SkipNavLink } from '@chakra-ui/skip-nav';
// import Container from "components/container"
import Footer from '@components/footer';
import Header from '@components/header';
// import SEO from '@components/seo';
import TableOfContent from '@components/table-of-content';
// import { convertBackticksToInlineCode } from "utils/convert-backticks-to-inline-code"
import PageTransition from './page-transition';
import SidebarRight from './sidebar-right';
import { FaGoogle } from 'react-icons/fa';

function useHeadingFocusOnRouteChange() {
  const router = useRouter();

  React.useEffect(() => {
    const onRouteChange = () => {
      const [heading] = Array.from(document.getElementsByTagName('h1'));
      heading?.focus();
    };
    router.events.on('routeChangeComplete', onRouteChange);
    return () => {
      router.events.off('routeChangeComplete', onRouteChange);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
}

interface PageContainerProps {
  frontmatter: {
    slug?: string;
    title: string;
    description?: string;
  };
  children: React.ReactNode;
  sidebar?: any;
  pagination?: any;
}

function PageContainer(props: PageContainerProps) {
  const { frontmatter, children, sidebar, pagination } = props;
  useHeadingFocusOnRouteChange();

  const { title, description } = frontmatter;

  return (
    <>
      {/* <SEO title={title} description={description} /> */}
      {/* <SkipNavLink zIndex={20}>Skip to Content</SkipNavLink> */}
      <Header />
      <Box as="main" className="main-content" w="full" maxW="8xl" mx="auto">
        <Box display={{ md: 'flex' }}>
          {sidebar || null}
          <Box flex="1" minW="0">
            <Box id="content" mx="auto" minH="76vh">
              <Flex>
                <Box
                  minW="0"
                  flex="auto"
                  px={{ base: '4', sm: '6', xl: '8' }}
                  pt="10"
                >
                  <PageTransition style={{ maxWidth: '48rem' }}>
                    <Heading
                      tabIndex={-1}
                      outline={0}
                      mb="32px"
                      fontSize={{ base: 'xl', md: '3xl' }}
                    >
                      {title}
                    </Heading>
                    {children}

                    <Flex justifyContent="center">
                      <Button
                        leftIcon={<FaGoogle />}
                        marginX="auto"
                        as={Link}
                        href="https://news.google.com/publications/CAAqBwgKMJK-nwswn8i3Aw?oc=3&ceid=UA:uk"
                        target="_blank"
                        mb="10"
                        size="lg"
                        backgroundColor="brand.300"
                      >
                        Читайте theageoffootball.com в Google News
                      </Button>
                    </Flex>

                    <div data-la-block="0a12afd1-a93d-4003-976c-6f37093d3132" />

                    <div className="r40532" />

                    <div id="kaqa-hiniqivovahuhapevano" />

                    <Box mt="40px">{pagination || null}</Box>
                    <Box pb="20">
                      <Footer />
                    </Box>
                  </PageTransition>
                </Box>
                <SidebarRight />
              </Flex>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default PageContainer;
