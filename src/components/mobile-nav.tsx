/* eslint-disable react-hooks/rules-of-hooks */
import React, {
  useEffect,
  useState,
  useRef,
  forwardRef,
  Ref,
  LegacyRef,
} from 'react';

import {
  Box,
  BoxProps,
  Center,
  CloseButton,
  Flex,
  HStack,
  IconButton,
  IconButtonProps,
  Text,
  useBreakpointValue,
  useColorModeValue,
  useTheme,
  useUpdateEffect,
} from '@chakra-ui/react';
import { AnimatePresence, motion, useElementScroll } from 'framer-motion';
import useRouteChanged from '@hooks/use-route-changed';
import { ReactChildren } from 'react';
import { AiOutlineMenu } from 'react-icons/ai';
import { RemoveScroll } from 'react-remove-scroll';
import { SidebarContent } from './sidebar/sidebar';
import MenuItems from './menu-items/menu-items';
import { useTranslations } from 'next-intl';

interface INavTabProps {
  isActive: boolean;
  children: ReactChildren | string;
  onClick: () => void;
}

const NavTab = ({ isActive, children, onClick }: INavTabProps) => (
  <Center
    flex="1"
    minH="40px"
    as="button"
    rounded="md"
    transition="0.2s all"
    fontWeight={isActive ? 'semibold' : 'medium'}
    bg={isActive ? 'brand.300' : undefined}
    borderWidth={isActive ? undefined : '1px'}
    color={isActive ? 'white' : undefined}
    _hover={{
      bg: isActive
        ? 'brand.300'
        : useColorModeValue('gray.100', 'whiteAlpha.100'),
    }}
    onClick={onClick}
    cursor="pointer"
  >
    {children}
  </Center>
);

interface MobileNavContentProps {
  isOpen?: boolean;
  onClose: () => void;
}

export function MobileNavContent(props: MobileNavContentProps) {
  const { isOpen, onClose } = props;
  const closeBtnRef = useRef<HTMLButtonElement>();

  const t = useTranslations('Common');

  useRouteChanged(onClose);

  const bgColor = useColorModeValue('white', 'gray.800');

  /**
   * Scenario: Menu is open on mobile, and user resizes to desktop/tablet viewport.
   * Result: We'll close the menu
   */
  const showOnBreakpoint = useBreakpointValue({ base: true, lg: false });

  useEffect(() => {
    if (showOnBreakpoint == false) {
      onClose();
    }
  }, [showOnBreakpoint, onClose]);

  useUpdateEffect(() => {
    if (isOpen) {
      requestAnimationFrame(() => {
        closeBtnRef.current?.focus();
      });
    }
  }, [isOpen]);

  const [shadow, setShadow] = useState<string>();
  const [activeTab, setActiveTab] = useState<'menu' | 'news'>('menu');

  return (
    <AnimatePresence>
      {isOpen && (
        <RemoveScroll forwardProps>
          <motion.div
            transition={{ duration: 0.08 }}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
          >
            <Flex
              direction="column"
              w="100%"
              bg={bgColor}
              h="100vh"
              overflow="auto"
              pos="absolute"
              top="0"
              left="0"
              zIndex={20}
              pb="8"
            >
              <Box>
                <Flex justify="space-between" px="6" pt="5" pb="4">
                  <Text
                    bgGradient="linear(to-l, #90ce7d,#2f5c21)"
                    bgClip="text"
                    fontSize="2xl"
                    fontFamily="logo"
                    fontWeight="extrabold"
                  >
                    Эпоха Футбола
                  </Text>
                  <HStack spacing="5">
                    <CloseButton
                      ref={
                        closeBtnRef as LegacyRef<HTMLButtonElement> | undefined
                      }
                      onClick={onClose}
                    />
                  </HStack>
                </Flex>
                <Box px="6" pb="6" pt="2" shadow={shadow}>
                  <HStack>
                    <NavTab
                      isActive={activeTab === 'menu'}
                      onClick={() => setActiveTab('menu')}
                    >
                      {t('categories') as string}
                    </NavTab>
                    <NavTab
                      isActive={activeTab === 'news'}
                      onClick={() => setActiveTab('news')}
                    >
                      {t('news') as string}
                    </NavTab>
                  </HStack>
                </Box>
              </Box>

              <ScrollView
                onScroll={(scrolled: any) => {
                  setShadow(scrolled ? 'md' : undefined);
                }}
              >
                {activeTab === 'menu' && <MenuItems />}
                {activeTab === 'news' && <SidebarContent />}
              </ScrollView>
            </Flex>
          </motion.div>
        </RemoveScroll>
      )}
    </AnimatePresence>
  );
}

const ScrollView = (props: BoxProps & { onScroll?: any }) => {
  const { onScroll, ...rest } = props;
  const [y, setY] = useState(0);
  const elRef = useRef<any>();
  const { scrollY } = useElementScroll(elRef);
  useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()));
  }, [scrollY]);

  useUpdateEffect(() => {
    onScroll?.(y > 5 ? true : false);
  }, [y]);

  return (
    <Box
      ref={elRef}
      flex="1"
      id="routes"
      overflow="auto"
      px="6"
      pb="6"
      {...rest}
    />
  );
};

// eslint-disable-next-line react/display-name
export const MobileNavButton = forwardRef(
  (props: IconButtonProps, ref: Ref<any>) => {
    return (
      <IconButton
        ref={ref}
        display={{ base: 'flex', lg: 'none' }}
        // @ts-ignore
        aria-label="Відкрити меню"
        fontSize="20px"
        color={useColorModeValue('gray.800', 'inherit')}
        variant="ghost"
        icon={<AiOutlineMenu />}
        {...props}
      />
    );
  }
);
