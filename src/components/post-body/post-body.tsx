import React from 'react';
import css from './post-body.module.css';

const PostBody = ({ content }: { content: string }) => (
  <div
    className={css.content}
    dangerouslySetInnerHTML={{ __html: content || '' }}
  />
);

export default PostBody;
