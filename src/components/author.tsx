import { Avatar, Box, Stack, Text } from '@chakra-ui/react';
import { parseISO, format } from 'date-fns';
import { ru } from 'date-fns/locale';
import Image from 'next/image';
import { IPostProps } from './post-list/post-list';

export const localizedFormat = (date: any, formatStr = 'PP') =>
  format(date, formatStr, {
    locale: ru,
  });

export function Author(props: IPostProps) {
  return (
    <>
      <Box
        borderRadius="full"
        overflow="hidden"
        w="48px"
        h="48px"
        pos="relative"
      >
        <Image
          src={props.author?.node?.avatar?.url}
          alt={props.author?.node.name}
          layout="fill"
        />
      </Box>
      <Stack direction="column" spacing={0} fontSize="sm">
        <Text fontWeight={600}>{props.author?.node.name}</Text>
        <Text color="gray.500">
          {localizedFormat(parseISO(props?.date), 'LLLL d, yyyy')}
        </Text>
        <style jsx>{`
          .avatar {
            border-radius: 50%;
            overflow: hidden;
          }
        `}</style>
      </Stack>
    </>
  );
}
