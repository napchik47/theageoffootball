import React from 'react';
import { Box, Text, Stack, chakra } from '@chakra-ui/react';
import { useTranslations } from 'next-intl';
import Link from 'next/link';

const UkraineFlag = (props: any) => (
  <chakra.svg
    display="inline-block"
    mx="3"
    h="16px"
    w="auto"
    viewBox="0 0 56 48"
    verticalAlign="middle"
    {...props}
  >
    <title>Ukraine</title>
    <g>
      <rect width="56" height="24" fill="#005BBB" />
      <rect width="56" height="24" y="24" fill="#FFD500" />
    </g>
  </chakra.svg>
);

export const Footer = () => {
  const t = useTranslations('Common');

  return (
    <Box as="footer" mt={12} textAlign="center">
      <Text fontSize="sm">
        <span>
          Proudly made in
          <UkraineFlag />
        </span>
      </Text>
      <Stack mt={4} direction="row" spacing="12px" justify="center">
        <Link href="/contacts" as="/contacts">
          <a>{t('contacts')}</a>
        </Link>
      </Stack>
    </Box>
  );
};

export default Footer;
