import { chakra, PropsOf, useColorModeValue } from '@chakra-ui/react';
import NextLink, { LinkProps } from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

const StyledLink = React.forwardRef(function StyledLink(
  props: PropsOf<typeof chakra.a> & { isActive?: boolean },
  ref: React.Ref<any>
) {
  const { isActive, children, href } = props;

  return (
    <chakra.a
      aria-current={isActive ? 'page' : undefined}
      href={href}
      width="100%"
      rounded="md"
      ref={ref}
      fontSize="sm"
      fontWeight="500"
      color={useColorModeValue('gray.700', 'whiteAlpha.900')}
      transition="all 0.2s"
      _activeLink={{
        color: useColorModeValue('brand.300', 'brand.300'),
        fontWeight: '600',
      }}
    >
      {children}
    </chakra.a>
  );
});

type SidebarLinkProps = React.PropsWithChildren<LinkProps>;

const SidebarLink = ({ children, ...props }: SidebarLinkProps) => {
  const { asPath } = useRouter();
  const isActive = asPath === props.href || asPath === props.as;

  return (
    <chakra.div
      userSelect="none"
      display="flex"
      alignItems="center"
      lineHeight="1.5rem"
    >
      <NextLink {...props} passHref>
        <StyledLink isActive={isActive}>{children}</StyledLink>
      </NextLink>
    </chakra.div>
  );
};

export default SidebarLink;
