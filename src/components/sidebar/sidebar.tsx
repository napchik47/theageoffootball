import dynamic from 'next/dynamic';
import { Box } from '@chakra-ui/react';

const PostFeed = dynamic(() => import('@components/post-feed'), {
  ssr: false,
});

export const SidebarContent = () => <PostFeed />;

const Sidebar = () => (
  <Box
    as="nav"
    aria-label="Головна навігація"
    pos="sticky"
    sx={{
      overscrollBehavior: 'contain',
    }}
    top="6.5rem"
    w="280px"
    h="calc(100vh - 8.125rem)"
    pr="8"
    pb="6"
    pl="6"
    pt="4"
    overflowY="auto"
    className="sidebar-content"
    flexShrink={0}
    display={{ base: 'none', md: 'block' }}
  >
    <SidebarContent />
  </Box>
);

export default Sidebar;
