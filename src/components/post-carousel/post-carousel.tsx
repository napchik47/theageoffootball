import CarouselItem from '@components/carousel-item/carousel-item';
import { IPostProps } from '@components/post-list/post-list';
import React from 'react';
import Carousel from 'react-multi-carousel';
import css from './post-carousel.module.css';

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1200 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1200, min: 577 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 576, min: 0 },
    items: 1,
  },
};

const PostCarousel = ({ posts }: { posts: Array<{ node: IPostProps }> }) => {
  return (
    <Carousel
      swipeable
      responsive={responsive}
      ssr
      showDots={false}
      slidesToSlide={1}
      autoPlay
      autoPlaySpeed={5000}
      transitionDuration={500}
      removeArrowOnDeviceType={['mobile']}
      infinite
      containerClass={css.carouselContainer}
      itemClass={css.imageItem}
      deviceType={''}
    >
      {posts?.map(({ node: postProps }: { node: IPostProps }, i: number) => (
        <CarouselItem
          key={postProps.id}
          {...postProps}
          priority={i === 0 ? true : false}
        />
      ))}
    </Carousel>
  );
};

export default PostCarousel;
