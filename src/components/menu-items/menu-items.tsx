import { useQuery, gql } from '@apollo/client';
import { chakra, PropsOf, useColorModeValue } from '@chakra-ui/react';
import { useTranslations } from 'next-intl';
import NextLink, { LinkProps } from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

export const PRIMARY_MENU = gql`
  query primaryMenu {
    menuItems(where: { location: PRIMARY }) {
      edges {
        node {
          id
          connectedObject {
            __typename
            ... on Post {
              id
              slug
              uri
              title
              contentType {
                node {
                  name
                }
              }
            }
            ... on Category {
              id
              name
              slug
              uri
              taxonomy {
                node {
                  name
                }
              }
            }
          }
          label
        }
      }
    }
  }
`;

const StyledLink = React.forwardRef(function StyledLink(
  props: PropsOf<typeof chakra.a> & { isActive?: boolean },
  ref: React.Ref<any>
) {
  const { isActive, children, href } = props;

  return (
    <chakra.a
      aria-current={isActive ? 'page' : undefined}
      href={href}
      width="100%"
      rounded="md"
      ref={ref}
      fontSize="sm"
      fontWeight="500"
      color={useColorModeValue('gray.700', 'whiteAlpha.900')}
      transition="all 0.2s"
      _activeLink={{
        color: useColorModeValue('brand.300', 'brand.300'),
        fontWeight: '600',
      }}
    >
      {children}
    </chakra.a>
  );
});

type NavLinkProps = React.PropsWithChildren<LinkProps>;

const NavLink = ({ children, ...props }: NavLinkProps) => {
  const { asPath } = useRouter();
  const isActive = asPath === props.href || asPath === props.as;

  return (
    <chakra.div
      userSelect="none"
      display="flex"
      alignItems="center"
      lineHeight="1.5rem"
    >
      <NextLink {...props} passHref>
        <StyledLink isActive={isActive}>{children}</StyledLink>
      </NextLink>
    </chakra.div>
  );
};

interface IMenuItemNode {
  node: {
    id: number;
    label: string;
    connectedObject?: {
      __typename: string;
      slug: string;
    };
  };
}

const MenuItems = () => {
  const t = useTranslations('Common');
  const { loading, error, data } = useQuery(PRIMARY_MENU);

  const menuItems = data?.menuItems?.edges;

  if (error) return <li>error</li>;
  if (loading) return <li>loading</li>;

  return (
    <>
      {menuItems.map(
        ({ node: { id, connectedObject, label } }: IMenuItemNode) => {
          if (!connectedObject) {
            return (
              <NavLink key={id} href={`/`}>
                <chakra.h4
                  fontSize="xs"
                  fontWeight="bold"
                  my="0.3rem"
                  textTransform="uppercase"
                  letterSpacing="wider"
                >
                  {label}
                </chakra.h4>
              </NavLink>
            );
          }

          const { __typename, slug } = connectedObject;

          const contentType = __typename.toLowerCase();

          return (
            <NavLink
              key={id}
              href={`/${contentType}/[slug]`}
              as={`/${contentType}/${slug}`}
            >
              <chakra.h4
                fontSize="xs"
                fontWeight="bold"
                my="0.3rem"
                textTransform="uppercase"
                letterSpacing="wider"
              >
                {label}
              </chakra.h4>
            </NavLink>
          );
        }
      )}
    </>
  );
};

export default MenuItems;
