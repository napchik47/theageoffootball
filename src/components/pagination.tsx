import { Box, Flex, Link, Text } from '@chakra-ui/react';
import { ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons';
import NextLink from 'next/link';
import React from 'react';

export const PaginationLink = (props: any) => {
  const { label, href, children, ...rest } = props;

  return (
    <NextLink href={href} passHref>
      <Link
        _hover={{
          textDecor: 'none',
        }}
        flex="1"
        borderRadius="md"
        {...rest}
      >
        <Text fontSize="sm" px="2">
          {label}
        </Text>
        <Box mt="1" fontSize="lg" fontWeight="bold" color="brand.300">
          {children}
        </Box>
      </Link>
    </NextLink>
  );
};

export const Pagination = ({ previous, next, ...rest }: any) => {
  return (
    <Flex
      as="nav"
      aria-label="Пагінація"
      spacing="40px"
      my="64px"
      flexDirection={{ base: 'column-reverse', md: 'row' }}
      justifyContent="space-between"
      {...rest}
    >
      {previous ? (
        <PaginationLink
          textAlign="left"
          label="Попередня"
          href={previous.path}
          rel="prev"
        >
          <Flex alignItems="flex-start">
            <ChevronLeftIcon mt="1" mr="1" fontSize="1.2em" />
            {previous.title}
          </Flex>
        </PaginationLink>
      ) : (
        <div />
      )}
      {next ? (
        <PaginationLink
          textAlign="right"
          label="Наступна"
          href={next.path}
          rel="next"
        >
          <Flex alignItems="flex-start">
            {next.title}
            <ChevronRightIcon mt="1" ml="1" fontSize="1.2em" />
          </Flex>
        </PaginationLink>
      ) : (
        <div />
      )}
    </Flex>
  );
};

export default Pagination;
