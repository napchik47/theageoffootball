import {
  chakra,
  Flex,
  HStack,
  IconButton,
  useColorMode,
  useColorModeValue,
  useDisclosure,
  useUpdateEffect,
  HTMLChakraProps,
  Text,
} from '@chakra-ui/react';
import { useViewportScroll } from 'framer-motion';
import NextLink from 'next/link';
import React, { useEffect, useRef, useState } from 'react';
import { FaMoon, FaSun } from 'react-icons/fa';
import { MobileNavButton, MobileNavContent } from './mobile-nav';
import MenuItems from './menu-items/menu-items';

function HeaderContent() {
  const mobileNav = useDisclosure();

  const { toggleColorMode: toggleMode } = useColorMode();
  const text = useColorModeValue('dark', 'light');
  const SwitchIcon = useColorModeValue(FaMoon, FaSun);
  const mobileNavBtnRef = useRef<HTMLButtonElement>();

  useUpdateEffect(() => {
    mobileNavBtnRef.current?.focus();
  }, [mobileNav.isOpen]);

  return (
    <>
      <Flex w="100%" h="100%" px="6" align="center" justify="space-between">
        <Flex align="center">
          <NextLink href="/" passHref>
            <chakra.a display="block" aria-label="Фрізмі">
              <Text
                bgGradient="linear(to-l, #90ce7d,#2f5c21)"
                bgClip="text"
                fontSize={{ base: '2xl', md: '3xl' }}
                fontFamily="logo"
                fontWeight="extrabold"
              >
                Эпоха Футбола
              </Text>
            </chakra.a>
          </NextLink>
        </Flex>

        <Flex
          justify="flex-end"
          w="100%"
          align="center"
          color="gray.400"
          maxW="1100px"
        >
          <HStack spacing="5" display={{ base: 'none', md: 'flex' }}></HStack>
          <HStack as={'nav'} spacing={4} display={{ base: 'none', md: 'flex' }}>
            <MenuItems />
          </HStack>
          <IconButton
            size="md"
            fontSize="lg"
            aria-label={`Перемикач тем`}
            variant="ghost"
            color="current"
            ml={{ base: '0', md: '3' }}
            onClick={toggleMode}
            icon={<SwitchIcon />}
          />
          <MobileNavButton
            ref={mobileNavBtnRef}
            aria-label="Відкрити меню"
            onClick={mobileNav.onOpen}
          />
        </Flex>
      </Flex>
      <MobileNavContent isOpen={mobileNav.isOpen} onClose={mobileNav.onClose} />
    </>
  );
}

function Header(props: HTMLChakraProps<'header'>) {
  const bg = useColorModeValue('white', 'gray.800');
  const ref = useRef<HTMLHeadingElement>();
  const [y, setY] = useState(0);
  const { height = 0 } = ref.current?.getBoundingClientRect() ?? {};

  const { scrollY } = useViewportScroll();
  useEffect(() => {
    return scrollY.onChange(() => setY(scrollY.get()));
  }, [scrollY]);

  return (
    <chakra.header
      ref={ref as React.LegacyRef<HTMLElement> | undefined}
      shadow={y > height ? 'sm' : undefined}
      transition="box-shadow 0.2s, background-color 0.2s"
      pos="sticky"
      top="0"
      zIndex="1001"
      bg={bg}
      left="0"
      right="0"
      width="full"
      {...props}
    >
      <chakra.div
        height={{ base: '3.5rem', md: '4.5rem' }}
        mx="auto"
        maxW="8xl"
      >
        <HeaderContent />
      </chakra.div>
    </chakra.header>
  );
}

export default Header;
