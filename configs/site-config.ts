const siteConfig = {
  copyright: `Copyright © ${new Date().getFullYear()} Eugene Domotenko. All Rights Reserved.`,
  author: {
    name: 'theageoffootball',
    email: 'theageoffootball.com@gmail.com',
  },
  seo: {
    socialBanner: 'https://theageoffootball.com/logo-light.svg',
    siteLogo: 'https://theageoffootball.com/logo-dark.svg',
    title: 'TheAgeOfFootball',
    titleTemplate: '%s - theageoffootball news',
    description:
      'Simple, Modular and Accessible UI Components for your React Applications.',
    siteUrl: 'https://theageoffootball.com',
    twitter: {
      handle: '@TheAgeOfFootball',
      site: '@TheAgeOfFootball',
      cardType: 'summary_large_image',
    },
    openGraph: {
      type: 'website',
      locale: 'en_US',
      url: 'https://theageoffootball.com',
      title: 'TheAgeOfFootball',
      description: 'Новини, які тобі сподобаються',
      site_name: 'TheAgeOfFootball',
      images: [
        {
          url: 'https://theageoffootball.com/logo-light.svg',
          width: 1240,
          height: 480,
          alt: 'Chakra UI: Simple, Modular and Accessible UI Components for your React Applications.',
        },
        {
          url: 'https://theageoffootball.com/logo-light.svg',
          width: 1012,
          height: 506,
          alt: 'Chakra UI: Simple, Modular and Accessible UI Components for your React Applications.',
        },
      ],
    },
  },
};

export default siteConfig;
